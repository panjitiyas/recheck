<?php

namespace App\Helpers;

use App\Models\Participant;
use Illuminate\Support\Facades\DB;

class CustomHelper {

    public static function getTotalParticipant()
    {
        return Participant::count();

    }
    public static function getTotalIdc($tanggal=null)
    {
        $data = DB::table('check_idc')->get()->count();
        if($tanggal!=null){
            $data = DB::table('check_idc')->whereDate('checkin_time',date('Y-m-d',strtotime($tanggal)))->get()->count();
        }

        return $data;

    }
    public static function getTotalReg($tanggal=null)
    {
        $data = DB::table('vcheckin')->whereNotNull('checkin_time')->get()->count();
        if($tanggal!=null){
            $data = DB::table('vcheckin')->whereDate('checkin_time',date('Y-m-d',strtotime($tanggal)))->get()->count();
        }

        return $data;

    }
    public static function getTotalExh($tanggal=null)
    {
        $data =  DB::table('vcheckin')->whereNotNull('check_in_pameran')->get()->count();
        if($tanggal!=null){
            $data = DB::table('vcheckin')->whereDate('check_in_pameran',date('Y-m-d',strtotime($tanggal)))->get()->count();
        }

        return $data;

    }

}