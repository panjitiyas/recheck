$(document).ready(function () {
    $("#btnTambah").on("click", function () {
        let title = "Tambah Data Peserta";
        let form = "/form";
        let button =
            '<a id="clsMd" href="#" class="px-5 rounded-1 btn btn-sm btn-secondary" >Batal</a>' +
            '<a id="simpanData" haref="#" class="px-5 rounded-1 btn btn-sm btn-dark">Simpan</a>';
        $("#mdTitle").html(title);
        $("#mdContent").load(form);
        $("#mdFooter").html(button);
        $("#modalTambah").modal("show");
        $("#clsMd").on("click", function () {
            $("#mdContent").empty();
            $("#modalTambah").modal("hide");
        });
        $("#simpanData").on("click", function (e) {
            e.preventDefault;
            let data = $("#formAdd").serialize();
            $.ajax({
                type: "post",
                url: "/participant",
                data: data,
                dataType: "json",
                success: function (data) {
                    if (data.code == 0) {
                         toastr.error(data.msg)
                    } else {
                        toastr.success(data.msg);
                        setTimeout(function () {
                            $("#tabel-peserta").DataTable().ajax.reload();
                            $("#formAdd")[0].reset();
                            $("#mdContent").empty();
                            $("#modalTambah").modal("hide");
                        }, 1500);

                    }
                },
            });
        });
    });


    $("#btnImpor").on("click", function () {
        let title = "Impor Data Peserta";
        let form = "/form-import";
        let button =
            '<a id="clsMd" href="#" class="px-5 rounded-1 btn btn-sm btn-secondary" >Batal</a>'
        $("#mdTitle").html(title);
        $("#mdContent").load(form);
        $("#mdFooter").html(button);
        $("#modalTambah").modal("show");
        $("#clsMd").on("click", function () {
            $("#mdContent").empty();
            $("#modalTambah").modal("hide");
        });
        $(document).on('submit','#importForm',function (e) {
            e.preventDefault()
            let icon = $('#iconStatus')
            icon.addClass('spinner-border text-light text-sm')
            let formData = new FormData(this);
            $.ajax({
                type: "post",
                url: "/import",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {
                    icon.removeClass('spinner-border text-light text-sm')
                    if (data.code == 0) {
                         toastr.error(data.msg)
                    } else {
                        toastr.success(data.msg);
                        setTimeout(function () {
                            $("#tabel-peserta").DataTable().ajax.reload();
                            $("#importForm")[0].reset();
                            $("#mdContent").empty();
                            $("#modalTambah").modal("hide");
                        }, 1500);

                    }
                },
                error: function(xhr, status, error) {
                    let errors = xhr.responseJSON.errors;
                    let errorHtml = '';

                    $.each(errors, function(key, value) {
                        errorHtml += '<p>' + value + '</p>';
                    });
                    toastr.error(errorHtml)
                }
            });
        });
    });


});
