<div class="row">
    <div class="col-md-10">
        <form id="frmFilter">
            <input type="date" name="tanggal" id="tanggal" class="form-control">
        </form>
    </div>
    <div class="col-md-2 py-3 py-md-0">
        <a class="btn btn-dark d-block" id="check">Check</a>
    </div>

</div>

<div class="row row-cols-1 mt-5">

    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <p class="mb-0 text-secondary">Pengambilan ID Card</p>
                        <h4 class="my-1" id="didc"></h4>
                        <p class="mb-0 text-success font-13">orang</p>
                    </div>
                    <div class="widget-icon-large bg-gradient-success text-white ms-auto"><i class="bi bi-person-badge-fill"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <p class="mb-0 text-secondary">Checkin Registrasi</p>
                        <h4 class="my-1" id="dreg"></h4>
                        <p class="mb-0 text-success font-13">orang</p>
                    </div>
                    <div class="widget-icon-large bg-gradient-danger text-white ms-auto"><i class="bi bi-person-check-fill"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <p class="mb-0 text-secondary">Checkin Exhibition</p>
                        <h4 class="my-1" id="dexh"></h4>
                        <p class="mb-0 text-success font-13">orang</p>
                    </div>
                    <div class="widget-icon-large bg-gradient-info text-white ms-auto"><i class="bi bi-person-bounding-box"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
