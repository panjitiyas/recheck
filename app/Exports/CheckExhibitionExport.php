<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet as WorksheetWorksheet;

class CheckExhibitionExport implements FromCollection, WithHeadings,WithMapping,WithStyles,ShouldAutoSize,WithTitle,WithEvents,WithCustomStartCell,WithColumnWidths
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;
    protected $tanggal;

    private $no = 0;
    public function __construct($tanggal)
    {
        $this->tanggal = $tanggal;
    }

    public function collection()
    {
        $column ='checkin_time';
        $table ='vcheckin';
        if(request()->segment(2)=='scanid'){
            $table ='vcheck';
        }
        if(request()->segment(1)=='exhib'){
            $column ='check_in_pameran';
        }
        if($this->tanggal!="all"){
            return DB::table($table)->whereDate($column,$this->tanggal)->get();
        } else {
            return DB::table($table)->whereNotNull($column)->get();
        }

    }

    public function map($data):array
    {
        $this->no++;
        $waktu = $data->checkin_time;
        if(request()->segment(1)=='exhib'){
            $waktu = $data->check_in_pameran;
        }
        return  [
            $this->no,
            $data->full_name,
            $data->company,
            $data->position,
            $waktu

        ];
    }
    public function startCell(): string
    {
        return 'A4';
    }
    public function headings(): array
    {
        return [
            'ID',
            'Nama',
            'Perusahaan/Organisasi',
            'Jabatan',
            'Waktu Checkin'
        ];
    }

    public function styles(WorksheetWorksheet $sheet)
    {
        return [
                    // Style the first row as bold text.
                    1    => ['font' => ['bold' => true]],
                    2    => ['font' => ['bold' => true]],
                    4    => ['font' => ['bold' => true]],
        ];

    }

    public function columnWidths(): array
    {
        return [
            'A' => 4.56,
        ];
    }

    public function title(): string
    {
        $title = "data_registrasi_peserta_";
        if(request()->segment(1)=='exhib'){
            $title = "data_hadir_pameran_";
        }
        return $title.date('ymd',strtotime(now()));
    }

    public function registerEvents(): array
    {

        return [
            BeforeSheet::class => function(BeforeSheet $event) {
                $title = "DATA REGISTRASI PESERTA";
                $tanggal = 'SAMPAI DENGAN TANGGAL '.date('d M Y',strtotime(now()));
                if (request()->segment(1) == 'exhib') {
                    $title = "DATA HADIR EXHIBITION ROOM";
                }
                if (request()->segment(2) == 'scanid') {
                    $title = "DATA PENGAMBILAN ID CARD";
                }
                if($this->tanggal!='all'){
                    $tanggal = 'PER TANGGAL - '.date('d M Y',strtotime($this->tanggal));
                }
                $event->sheet->setCellValue('A1', $title);
                $event->sheet->setCellValue('A2', $tanggal);
            },
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getStyle('A4:E4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            },
        ];
    }

}
