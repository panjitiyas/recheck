<script>
    $(function() {
        getGeneral()
        getDaily()


        function getGeneral() {
            $.ajax({
                type: "GET"
                , url: "./get-general"
                , dataType: "json"
                , success: function(data) {
                    $('#gtptc').text(data.ptc)
                    $('#gtidc').text(data.idc)
                    $('#gtreg').text(data.reg)
                    $('#gtexh').text(data.exh)

                }
            });
        }

        function getDaily(){
            $.ajax({
                type: "GET"
                , url: "./get-daily"
                , dataType: "json"
                ,data :{
                    'tanggal':'all'
                }
                , success: function(data) {
                    $('#didc').text(data.idc)
                    $('#dreg').text(data.reg)
                    $('#dexh').text(data.exh)
                }
            });
        }
        $('#check').on('click', function() {
            form = $('#frmFilter')
            $.ajax({
                type: "GET"
                , url: "./get-daily"
                , data: form.serialize()
                , dataType: "json"
                , success: function(data) {
                    $('#didc').text(data.idc)
                    $('#dreg').text(data.reg)
                    $('#dexh').text(data.exh)

                }
            });
        })

        $('#fetchData').on('click',function(){
            getGeneral()
        })

    })

</script>

