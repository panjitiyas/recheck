$(function(){
    let progressBar = $('#progress-bar');
    $('#blastEmail').on('click',function(){
       $('#icon-mail').addClass('spinner-border text-light text-sm')
       $('#icon-mail').removeClass('bx bx-mail-send')
       $.ajax({
        type:'GET',
        url:'/send-mail',
        dataType:'json',
        success:function(data){
            if(data.code==1){
                toastr.success(data.msg);
                $("#tabel-peserta").DataTable().ajax.reload();
                $("#icon-mail").removeClass("spinner-border text-light text-sm");
                $("#icon-mail").addClass("bx bx-mail-send");
            } else{
                toastr.error(data.msg)
                $("#tabel-peserta").DataTable().ajax.reload();
                $("#icon-mail").removeClass("spinner-border text-light text-sm");
                $("#icon-mail").addClass("bx bx-mail-send");
            }
        },
        error:function(){
            toastr.error('Terjadi Kesalahan')
        }
       });

       let intervalId = setInterval(function() {
           $.ajax({
               type: 'GET',
               url: '/check-progress',
               success: function(response) {
                   // Mengupdate progres
                   progressBar.width(response.progress + '%');

                   // Ketika progres mencapai 100%, berhenti memantau
                   if (response.progress >= 100) {
                       clearInterval(intervalId);
                   }
               },
               error: function() {
                   alert('Terjadi kesalahan saat memeriksa progres.');
                   clearInterval(intervalId);
               }
           });
       }, 1000);
    })


})