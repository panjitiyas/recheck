<?php

namespace App\Http\Controllers;

use App\Models\Participant;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Participant::orderBy('id', 'asc')->with(['genders', 'entities']);
        return DataTables::eloquent($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return view('pages.participant.komponen.tombol')->with('data', $data);
            })
            ->editColumn('full_name', function ($data) {
                return ucwords($data->full_name);

            })
            ->editColumn('sts_email',function($data){
                if ($data->sts_email==0) return '<i class="bx bx-envelope text-danger"></i>';
                return '<i class="bx bx-envelope text-success"></i>';
            })->toJson();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $code = 0;
        $msg = "Data gagal disimpan";


        $lastId = Participant::orderBy('id', 'desc')->first();
        if($lastId==null){
            $currentId =0;
        }else{
            $currentId = $lastId->uuid;
        }

        $newId = $currentId + 1;

        $rules = [
            'nama' => 'required',
            'company' => 'required',
            'jabatan' => 'required',
            'email' => 'required|email',
            'noWa' => 'required|regex:/^628[0-9]{9,13}$/'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $code = 0;
            $msg = "Data gagal disimpan";
        } else {
            $code = 1;
            $msg = "Data berhasil disimpan";
            $uuid = $newId;
            $folder = 'upload/qrCodes/'.$uuid.'_'.strtolower(substr(str_replace(' ','_',$request->nama),0,10))  . '/';
            $folder2 = 'upload/invit/'.$uuid.'_'.strtolower(substr(str_replace(' ','_',$request->nama),0,10))  . '/';
            if (!File::exists(public_path($folder))) {
                File::deleteDirectory(public_path($folder));
                File::makeDirectory(public_path($folder),0777,true);
            }
            if (!File::exists(public_path($folder2))) {
                File::deleteDirectory(public_path($folder2));
                File::makeDirectory(public_path($folder2),0777,true);
            }
            $genData = array(
                'name'=>$request->nama,
                'company'=>$request->company,
                'position'=>$request->jabatan,
                'wa_number'=>$request->noWa,
                'email'=>$request->email,
                'uuid'=>$uuid,
                'folder'=>$folder,
                'folder2'=>$folder2
            );


            $data = new Participant;
            $data->uuid = $uuid;
            $data->full_name = $request->nama;
            $data->sex = $request->sex;
            $data->entity = $request->entitas;
            $data->company = $request->company;
            $data->position = $request->jabatan;
            $data->email = $request->email;
            $data->wa_number = $request->noWa;
            $data->path_qrId = $this->generateAndSaveQRCode($genData);
            $data->path_qrVc =$this->generateQrVcard($genData);
            $data->save();
            // Validasi berhasil
            // Lakukan tindakan yang diinginkan dengan nomor handphone yang valid
        }
        $respon = array(
            'code' => $code,
            'msg' => $msg
        );

        echo json_encode($respon);
    }


    public function generateAndSaveQRCode($data)
    {
        $path = $data['folder'];
        $filename = 'uuid_' . $data['uuid'] . '_' . substr(str_replace(' ', '_', Str::lower($data['name'])),0,16) . '.png';


        $file_path = $path . $filename;
        if (File::exists(public_path($file_path))) {
            File::delete(public_path($file_path));
        }
        // Generate QR Code
        QrCode::format('png')->size(300)->margin(5)->generate($data['uuid'], public_path($file_path));

        // // Make Latar

        // $latar = Image::make(asset('assets/images/invit.png'));

        // // Membuka gambar QR code
        // $qrCodeImage = public_path($file_path);

        // // Menggabungkan gambar QR code dengan latar belakang
        // $latar->insert($qrCodeImage, 'bottom-left',270,110);

        // // Simpan gambar hasil
        // $path2 = $data['folder2'];
        // $filename2 = 'invit_' . $data['uuid'] . '_' . substr(str_replace(' ', '_', Str::lower($data['name'])), 0, 16) . '.png';
        // $file_path2 = $path2 . $filename2;
        // $latar->save($file_path2);

        return $filename;
    }

    public function generateQrVcard($data)
    {
        $folder = $data['folder'];
        $filename = 'vcard_' . $data['uuid'] . '_' . substr(str_replace(' ', '_', Str::lower($data['name'])),0,16) . '.png';


        $file_path = $folder . $filename;
        if (File::exists(public_path($file_path))) {
            File::delete(public_path($file_path));
        }

        $vcardString = 'BEGIN:VCARD' . PHP_EOL;
        $vcardString .= 'VERSION:3.0' . PHP_EOL;
        $vcardString .= 'N:' . $data['name'] . ';' . $data['name'] . ';;;' . PHP_EOL;
        $vcardString.= 'FN:'.$data['name']. PHP_EOL;
        $vcardString.= 'ORG:'.$data['company']. PHP_EOL;
        $vcardString.= 'TITLE:'.$data['position']. PHP_EOL;
        $vcardString .= 'EMAIL;TYPE=INTERNET:' . $data['email'] . PHP_EOL;
        $vcardString .= 'TEL;TYPE=CELL:' . $data['wa_number'] . PHP_EOL;
        $vcardString .= 'END:VCARD';

        // Generate QR Code
        QrCode::format('png')->size(300)->margin(5)->generate($vcardString, public_path($file_path));


        return $filename;
    }


    public function genAllQr(){

        $code = 0;
        $message ='Tidak ada data untuk generate Qr';
        $persons = Participant::whereNull('path_qrId') // Hanya email dengan status 0
        ->get();
        $jumlahData = $persons->count();
        $count = 0;

        if($jumlahData > 0){
            foreach ($persons as $person){
                $folder = 'upload/qrCodes/'.strtolower($person->uuid.'_'.substr(str_replace(' ','_',$person->full_name),0,10) ) . '/';
                if (!File::exists(public_path($folder))) {
                    File::deleteDirectory(public_path($folder));
                    File::makeDirectory(public_path($folder),0777,true);
                }
                $genData = array(
                    'name'=>$person->full_name,
                    'company'=>$person->company,
                    'position'=>$person->position,
                    'wa_number'=>$person->wa_number,
                    'email'=>$person->email,
                    'uuid'=>$person->uuid,
                    'folder'=>$folder,
                );
                $pathQr = $this->generateAndSaveQRCode($genData);
                $pathVc=$this->generateQrVcard($genData);
                Participant::where('id',$person->id)->update(['path_qrId'=>$pathQr,'path_qrVc'=>$pathVc]);
                $count++;
            }
            if($count == $jumlahData){
                $code =1;
                $message ='Proses Berhasil';
            } else {
                $code=0;
                $message=$jumlahData - $count.' data tidak berhasil diproses.';
            }
        }

        $data = array(
            'code'=>$code,
            'msg'=>$message
        );

        return json_encode($data);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $data = Participant::find($id);

        // Mengembalikan view dengan data yang akan diedit
        return view('pages.participant.komponen.form-edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Participant $participant)
    {
        $code = 0;
        $msg = "Terjadi Kesalahan";

        $rules = [
            'nama' => 'required',
            'company' => 'required',
            'jabatan' => 'required',
            'email' => 'required|email',
            'noWa' => 'required|regex:/^628[0-9]{9,13}$/'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $code = 0;
            $msg = "Data gagal diupdate";
        } else {
            $participant->update([
                'full_name'=>$request->nama,
                'sex'=>$request->sex,
                'entity'=>$request->entitas,
                'company'=>$request->company,
                'position'=>$request->jabatan,
                'email'=>$request->email,
                'wa_number'=>$request->noWa,
            ]);
            $code = 1;
            $msg = "Data ".$participant->full_name." berhasil diupdate";
        }

        $data =array(
            'code'=>$code,
            'msg'=>$msg
        );

        return json_encode($data);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $code = 0;
        $msg = 'Gagal menghapus data';

        try {
            $data = Participant::find($id);

            if ($data) {
                // dd('menghapus data');
              $data->delete();
              $code = 1;
              $msg = 'Data berhasil dihapus';
            } else {
              return response()->json(['message' => 'Data tidak ditemukan']);
              $msg = 'Data tidak ditemukan';
            }
          } catch (\Exception $e) {
            $msg = 'Terjadi kesalahan saat menghapus data';
          }

          $response = array(
            'code'=>$code,
            'msg'=>$msg
          );

          return json_encode($response);
    }
}
