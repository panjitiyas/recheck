@extends('layouts.app')
@section('content')
<div class="row ${1| ,row-cols-2,row-cols-3, auto,justify-content-md-center,|}">
    <div class="col-12">
        <h5>REGISTRASI ID CARD</h5>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h4>Ekspor Data</h4>
    </div>
    <div class="card-body">
        <div class="col-12 justify-content-end">
            <div class="row">
                <div class="col-md-8">
                    <input type="date" name="tanggal" id="tanggal" class="form-control form-control-sm">

                </div>
                <div class="col-md-4">
                    <a href="./export" id="btnEkspor"  class="btn btn-sm btn-dark"><i id="icon-ex" class="bx bx-download"></i> Expor Data</a>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="card shadow-sm radius-10 border-0 mb-3">
    <div class="card-body">
        <h4>Data Peserta</h4>
        <div class="table-responsive">
            <table id="tabel-idcard" class="table table-sm table-hover table-striped" width="100%">
                <thead class="text-center">
                    <th>#</th>
                    <th>Nama</th>
                    <th width="150">Perusahaan</th>
                    <th width="150">Jabatan</th>
                    <th>Waktu Pengambilan</th>
                    <th>User Operator</th>
                    <th>Kelola</th>
                </thead>
            </table>

        </div>
    </div>
</div>


@endsection

@section('script')
<script src={{ asset('assets/pages/regscan/js/tayang-data.js') }}></script>
<script src={{ asset('assets/pages/regscan/js/tambah-data.js') }}></script>
<script>
    // ==============================================//

    $('#btnEkspor').on('click',function(e){
    e.preventDefault()

    let data = $('#expData')

    $.ajax({
        xhrFields: {
        responseType: 'blob',
        },
        type:'GET',
        url:'./scanid/export',
        data:data.serialize(),
        success:function(result, status, xhr){
            var disposition = xhr.getResponseHeader('content-disposition');
            var matches = /"([^"]*)"/.exec(disposition);
            var filename = (matches != null && matches[1] ? matches[1] : 'yuks.xlsx');

            // The actual download
            var blob = new Blob([result], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            });
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = filename;

            document.body.appendChild(link);

            link.click();
            document.body.removeChild(link);


        }
    })
})
</script>
@endsection


