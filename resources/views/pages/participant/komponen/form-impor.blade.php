<form id="importForm" enctype="multipart/form-data">
    @csrf
    <input type="file" name="impor_file" class="form-control form-control-file">
    <br>
    <button id="btnImport" class="btn btn-dark btn-sm px-5" type="submit"><div id="iconStatus"></div> Unggah Data</button>
    <a href={{ asset('upload/template.xlsx') }} class="px-5 btn btn-outline-dark btn-sm" type="submit">Unduh Template</a>
</form>