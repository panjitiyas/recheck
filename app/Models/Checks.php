<?php

namespace App\Models;

use App\Models\Participant;
use Illuminate\Database\Eloquent\Model;
use PhpOffice\PhpSpreadsheet\Calculation\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Checks extends Model
{
    use HasFactory;
    protected $fillable=
    [
        'participant_id',
        'category_id',
        'checkin_time',
        'user',
        'status'

    ];

    public function participant()
    {
        return $this->hasMany(Participant::class,'id');
    }
    public function category()
    {
        return $this->hasMany(Category::class,'id');
    }
}
