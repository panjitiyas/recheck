@extends('layouts.app')
@section('content')
<div class="row ${1| ,row-cols-2,row-cols-3, auto,justify-content-md-center,|}">
    <div class="col-12">
        <h5>REGISTRASI EVENT</h5>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h4>Ekspor Data</h4>
    </div>
    <div class="card-body">
        <div class="col-12 justify-content-end">
            <div class="row">
                <div class="col-md-8">
                    <input type="date" name="tanggal" id="tanggal" class="form-control form-control-sm">

                </div>
                <div class="col-md-4">
                    <a href="./export" id="btnEkspor"  class="btn btn-sm btn-dark"><i id="icon-ex" class="bx bx-download"></i> Expor Data</a>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <ul class="nav nav-tabs nav-primary" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" data-bs-toggle="tab" href="#primaryhome" role="tab" aria-selected="true">
                    <div class="d-flex align-items-center">
                        <div class="tab-icon"><i class="bx bx-home font-18 me-1"></i>
                        </div>
                        <div class="tab-title">By QR</div>
                    </div>
                </a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" data-bs-toggle="tab" href="#primaryprofile" role="tab" aria-selected="false" tabindex="-1">
                    <div class="d-flex align-items-center">
                        <div class="tab-icon"><i class="bx bx-user-pin font-18 me-1"></i>
                        </div>
                        <div class="tab-title">By Name</div>
                    </div>
                </a>
            </li>
        </ul>
        <div class="tab-content py-3">
            <div class="tab-pane fade active show" id="primaryhome" role="tabpanel">
                {{-- BY QR --}}

                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="ms-auto position-relative">
                                <div class="position-absolute top-50 translate-middle-y search-icon fs-5 px-3"><i class="bx bx-scan"></i></div>
                                <input id="regScanEv" name="uuid" class="form-control form-control-lg ps-5" type="text" placeholder="Pindai QR">
                            </div>
                            <!--end row-->
                        </div>
                    </div>
                </div>


            </div>
            <div class="tab-pane fade" id="primaryprofile" role="tabpanel">
                {{-- BY NAMA --}}

                <h4>Data Peserta</h4>
                <div class="table-responsive">
                    <table id="tabel-guest" class="table table-sm table-hover table-striped" width="100%">
                        <thead class="text-center">
                            <th>#</th>
                            <th>Nama</th>
                            <th width="150">Perusahaan</th>
                            <th width="150">Jabatan</th>
                            <th>Aksi</th>
                        </thead>
                    </table>

                </div>
            </div>
        </div>

        <div class="row py-3">
            <div class="col-12">
                <h4>Data Checkin</h4>
                <div class="table-responsive">
                    <table id="tabel-checkin" class="table table-stripe" width="100%">
                        <thead class="text-center">
                            <th>#</th>
                            <th>Nama</th>
                            <th width="150">Perusahaan</th>
                            <th width="150">Jabatan</th>
                            <th>Waktu Registrasi</th>
                            <th>User Operator</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>









@endsection

@section('script')
<script>
    $(document).ready(function() {


        // Tayang Data
        $('#tabel-checkin').DataTable({
            processing: true
            , serverside: true,
            stateSave:true
            , ajax: './data-check'
            , columns: [{
                    data: "DT_RowIndex"
                    , name: "DT_RowIndex"
                    , orderable: false
                    , searchable: false
                , }
                , {
                    data: "full_name"
                    , name: "nama"
                , }
                , {
                    data: "company"
                    , name: "Perusahaan"
                , }
                , {
                    data: "position"
                    , name: "Jabatan"
                , }
                , {
                    data: "checkin_time"
                    , name: 'checkin'

                }
                , {
                    data: "user"
                    , name: 'user'
                }
            ],

            columnDefs: [{
                render: function(data, type, full, meta) {
                    if (data == null) return "<div class='text-center'> - </div>";
                    return "<div class='text-center'>" + data + "</div>";
                }
                , targets: [0, 4, 5]
            }]
        })

// ===========================================//

        $("#tabel-guest").DataTable({
                processing: true,
                serverside: true,
                pageLength: 5,
                lengthMenu: [ 5,10, 25, 50, 75, 100 ],
                stateSave:true,
                ajax:'./check-name',
                columns: [
                    {
                        data: "DT_RowIndex",
                        name: "DT_RowIndex",
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: "full_name",
                        name:"nama",
                    },
                    {
                        data: "company",
                        name: "Perusahaan",
                    },
                    {
                        data: "position",
                        name: "Jabatan",
                    },
                    {
                        data: "aksi",
                        name: "aksi",
                    },
                ],
                columnDefs: [
                    {
                        render: function (data, type, full, meta) {
                            return "<div class='text-wrap'>" + data + "</div>";
                        },
                        targets: [2,3]
                    },
                    {
                        render: function (data, type, full, meta) {
                            if(data==null) return "<div class='text-center'> - </div>";
                            return "<div class='text-center'>" + data + "</div>";
                        },
                        targets: [0,4]
                    }
                ]
        });


        let input2 = $('#regbyName')
        let input = $('#regScanEv')
        input.on('keyup', function(event) {
            let id = input.val()
            let id2 = input2.val()

            let audioPath = ' {{ asset("assets/audio/beep-24.mp3") }}';
            let audio = new Audio(audioPath);


            if (event.keyCode === 13) {
                $(this).val("");
                    $.ajax({
                        type: 'GET'
                        , url: './checkin'
                        , data: {
                            'id': id
                        , }
                        , dataType: 'json'
                        , success: function(data) {
                            if (data.code == 0) {
                            audio.play()
                            toastr.error(data.msg)
                            setTimeout(function () {

                            $('#tabel-checkin').DataTable().ajax.reload()

                            }, 1500);

                            } else {
                            audio.play()
                            toastr.success(data.msg)
                            setTimeout(function () {
                            $('#tabel-checkin').DataTable().ajax.reload()
                            }, 1500);

                            }
                        }
                    })
            }
        })

        $($('#tabel-guest')).on('click','#bCheck',function(){
            let id = $(this).data('id')
            let audioPath = ' {{ asset("assets/audio/beep-24.mp3") }}';
            let audio = new Audio(audioPath);
            console.log((id))
        $.ajax({
            type: 'GET'
            , url: './checkin',

            data: {
                'id':id,
            },
            dataType: "json",
            success: function (data) {

                if (data.code == 0) {
                     toastr.error(data.msg)
                     audio.play()
                    setTimeout(function () {
                        $("#tabel-checkin").DataTable().ajax.reload();
                    }, 1500);
                } else {
                    toastr.success(data.msg);
                    audio.play()
                    setTimeout(function () {
                        $("#tabel-checkin").DataTable().ajax.reload();
                    }, 1500);
                }
            },
            error:function(data){
                toastr.error('Terjadi Kesalahan')
            }
        });
        })

    })

    // ==============================================//

    $('#btnEkspor').on('click',function(e){
    e.preventDefault()

    let data = $('#expData')

    $.ajax({
        xhrFields: {
        responseType: 'blob',
        },
        type:'GET',
        url:'./export',
        data:data.serialize(),
        success:function(result, status, xhr){
            var disposition = xhr.getResponseHeader('content-disposition');
            var matches = /"([^"]*)"/.exec(disposition);
            var filename = (matches != null && matches[1] ? matches[1] : 'yuks.xlsx');

            // The actual download
            var blob = new Blob([result], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            });
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = filename;

            document.body.appendChild(link);

            link.click();
            document.body.removeChild(link);


        }
    })
})

</script>

@endsection

