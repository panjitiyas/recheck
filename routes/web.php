<?php

use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ImporController;
use App\Http\Controllers\ChecksController;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Http\Controllers\SendMailController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ParticipantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// LOGIN
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
Route::get('/login',function(){
    return view('auth.login');
});


Route::middleware(['auth'])->group(function () {
    // DASHBOARD
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/get-general', [DashboardController::class, 'generalStats']);
    Route::get('/get-daily', [DashboardController::class, 'dailyStats']);

    // PARTICIPANT
    Route::get('/participants', function () {
        return view('pages.participant.index');
    });
    Route::resource('participant', ParticipantController::class);
    Route::get('/form',function(){
    return view('pages.participant.komponen.form');
    });
    Route::get('/form-import',function(){
    return view('pages.participant.komponen.form-impor');
    });
    // REGISTRASI
    Route::get('/regist/scan', function () {
        return view('pages.registrasi.checkin_reg');
    });
    Route::get('/regist/checkin',[ChecksController::class,'checkEvent']);
    Route::get('/regist/data-check',[ChecksController::class,'index']);
    Route::get('/regist/check-name',[ChecksController::class,'getDataCheckByName']);
    Route::get('/regist/export',[ChecksController::class,'ExportData']);

    // REGISTRASI ID
    Route::get('/regist/scanid', function () {
        return view('pages.registrasi.checkin_id');
    });

    Route::get('/regist/scanid/export',[ChecksController::class,'ExportData']);


    Route::get('/update-id',[ChecksController::class,'takeIdcard']);
    Route::get('/regist/data-idc',[ChecksController::class,'getDataParticipantforIdcard']);

    // EXHIBITION
    Route::get('/exhib/scan', function () {
        return view('pages.pameran.checkin_reg');
    });
    Route::get('/exhib/checkin',[ChecksController::class,'checkEvent']);
    Route::get('/exhib/data-check',[ChecksController::class,'index']);
    Route::get('/exhib/check-name',[ChecksController::class,'getDataCheckByName']);
    Route::get('/exhib/export',[ChecksController::class,'ExportData']);


    // KIRIM EMAIL
    Route::get('/send-mail',[SendMailController::class,'sendMail']);
    Route::get('/check-progress',[SendMailController::class,'checkProgressEmail']);

    // GEN QR
    Route::get('/gen-qr',[ParticipantController::class,'genAllQr']);


    route::get('/fullsc',function(){
        return view('pages.registrasi.fullsc');
    });
});

Route::post('/import', [ImporController::class,'import'])->name('import');
Route::get('/export', [ImporController::class,'export'])->name('export');

Route::get('/php', fn () => phpinfo());

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

Route::get('/cekformat',function(){
    $data = [
        'attachment' => public_path('/upload/Term of Reference IER Conference 2023 for Participants.pdf')
    ];

    $email = new WelcomeMail($data);
    Mail::to('panjitiyas@gmail.com')->send($email);
    return 'Berhasil terkirim';
});


