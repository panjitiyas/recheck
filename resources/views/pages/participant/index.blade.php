@extends('layouts.app')

@section('content')
<div class="card shadow-sm radius-10 border-0 mb-3">
    <div class="card-body">
        <div class="col-12 justify-content-end">
            <a href="#" id="btnTambah" class="btn btn-sm btn-dark"><i class="bx bx-plus"></i> Tambah Data</a>
            <a href="#" id="btnImpor"  class="btn btn-sm btn-dark"><i class="bx bx-upload"></i> Impor Data</a>
            <a href="export" id="btnEkspor"  class="btn btn-sm btn-dark"><i id="icon-ex" class="bx bx-download"></i> Expor Data</a>
            <a href="#" id="btnQr"  class="btn btn-sm btn-dark"><i id="icon-qr" class="bx bx-play-circle"></i> Generate QR</a>
            <a href="#" id="blastEmail"  class="btn btn-sm btn-dark"><i id="icon-mail" class="bx bx-mail-send"></i> Send Email</a>
        </div>
    </div>
</div>
<div class="progress">
    <div id="progress-bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
</div><br>
<div class="card shadow-sm radius-10 border-0 mb-3">
    <div class="card-body">
        <h4>Data Peserta</h4>
        <div class="table-responsive">
            <table id="tabel-peserta" class="table table-sm table-hover table-striped" width="100%">
                <thead class="text-center">
                    <th>#</th>
                    <th width="200">Nama</th>
                    <th width="250">Perusahaan</th>
                    <th width="250">Jabatan</th>
                    <th>Nomor WA</th>
                    <th width="150">Email</th>
                    <th>Aksi</th>
                </thead>
            </table>

        </div>
    </div>
</div>
@include('pages.participant.komponen.modal')
@include('pages.participant.komponen.modal-hapus')
@endsection

@section('script')
@include('pages.participant.script')
@endsection
