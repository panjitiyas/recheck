<!doctype html>
<html lang="en">
<head>
    <title>Login reCheck</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href={{ asset('assets/login/css/style.css') }}>
    <link rel="stylesheet" href={{ asset('assets/css/toastr.css') }}>

</head>
<body>
    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 text-center mb-5">
                    <h3 class="heading-section"><span class="font-weight-bold">re::</span>Check</h3>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-5">
                    <div class="login-wrap p-4 p-md-5">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-user-o"></span>
                        </div>
                        <form id="login-form" class="login-form">
                            <div class="form-group">
                                <input name="username" type="text" class="form-control rounded-left" placeholder="Username" required>
                            </div>
                            <div class="form-group d-flex">
                                <input name="password" type="password" class="form-control rounded-left" placeholder="Password" required>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary rounded submit p-3 px-5">Get Started</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src={{ asset('assets/js/jquery.min.js') }}></script>
    <script src={{ asset('assets/js/bootstrap.bundle.min.js') }}></script>
    <script src={{ asset('assets/js/toastr.js')}}></script>
    <script src={{ asset('assets/login/js/main.js') }}></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#login-form').submit(function(e) {
                e.preventDefault();
                let data = $(this).serialize();
                $.ajax({
                    url: '{{ route("login") }}'
                    , type: "POST"
                    , dataType: 'json'
                    , data: data
                    , success: function(response) {
                        if (response.success) {
                            toastr.success(response.message)
                            setTimeout(() => {
                                window.location.href = "/";
                            }, 1500);
                        } else {
                            toastr.error(response.message)
                        }
                    }
                    , error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                })
            });
        })

    </script>

</body>
</html>

