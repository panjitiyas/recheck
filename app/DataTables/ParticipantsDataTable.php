<?php

namespace App\DataTables;

use App\Models\Participant;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ParticipantsDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query));
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Participant $model): QueryBuilder
    {
        return $model->newQuery()->with(['genders', 'entities']);
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('participants-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            // ->dom('Bfrtip')
            ->orderBy(1)
            ->selectStyleSingle();
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('uuid')->title('Id')->addClass('text-center'),
            Column::make('full_name')->title('Nama')->addClass('text-center'),
            // Column::make('genders.name')->title('Gender'),
            // Column::make('entities.name')->title('Entitas')->addClass('text-center'),
            Column::make('company')->title('Perusahaan')->addClass('text-center'),
            Column::make('position')->title('Jabatan')->addClass('text-center'),
            // Column::make('wa_number')->title('Nomor WA')->addClass('text-center'),
            // Column::make('email'),
            Column::make('qrId')->title('ID Card')->addClass('text-center'),
            Column::make(
                [
                    'defaultContent' => '<button type="button" class="btn d-sm-block btn-sm btn-primary split-bg-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown">Menu</button>
                    <ul class="dropdown-menu" style="">
                            <li><a class="dropdown-item" href="#">Ubah</a>
                            </li>
                            <li><a class="dropdown-item" href="#">Hapus</a>
                            </li>
                          </ul>',
                    'data'           => 'action',
                    'name'           => 'action',
                    'title'          => 'Aksi',
                    'class'         => 'text-center',
                    'width'         =>  '80',
                    'render'         => null,
                    'orderable'      => false,
                    'searchable'     => false,
                    'exportable'     => false,
                    'printable'      => true,
                    'footer'         => '',
                ]
            )
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Participants_' . date('YmdHis');
    }
}
