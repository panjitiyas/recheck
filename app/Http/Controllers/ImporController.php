<?php

namespace App\Http\Controllers;

use App\Exports\ParticipantExport;
use Illuminate\Http\Request;
use App\Imports\ParticipantImport;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Validators\ValidationException;

class ImporController extends Controller
{
    public function import(Request $request)
    {
        $code = 0;
        $msg = 'Data gagal diunggah';

        try {
            $file = $request->file('impor_file');
            $filename = $file->getClientOriginalName();
            $file->move('data_peserta',$filename);
            Excel::import(new ParticipantImport, public_path('/data_peserta/'.$filename));

            $code = 1;
            $msg = 'Data berhasil diunggah';
        } catch (ValidationException $e) {
            $failures = $e->failures();

            foreach ($failures as $failure) {
                $errors[] = 'Row ' . $failure->row() . ': ' . $failure->errors()[0];
            }
            $code = 0;
            $msg = $errors;
        }

        $data = array (
            'code'=>$code,
            'msg'=>$msg
        );

        return json_encode($data);
    }
    public function export()
    {
       return Excel::download(new ParticipantExport,'data_peserta'.now().'.xlsx');
    }
}
