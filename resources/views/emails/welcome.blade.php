<style>
.btn {
  display: inline-block;
  font-weight: 400;
  line-height: 1.5;
  color: #212529;
  text-align: center;
  text-decoration: none;
  vertical-align: middle;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;
  background-color: transparent;
  border: 1px solid transparent;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  border-radius: 0.25rem;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}

.btn-primary {
  color: #fff;
  background-color: #0d6efd;
  border-color: #0d6efd;
}
</style>
<div>
    Yth. Bapak/Ibu,<br><br>

    Selamat anda telah berhasil melakukan pendaftaran peserta pada acara <b>"Industrial And Employee Relations Conference 2023 - Forum Human Capital Indonesia.</b> <br><br>
    Pengambilan ID-Card dan <i>Goodybag</i>  dilaksanakan pada : <br><br>

    <table>
        <tr>
            <td>Hari/Tanggal</td>
            <td>:</td>
            <td>Rabu, 23 Agustus 2023</td>
        </tr>
        <tr>
            <td>Waktu</td>
            <td>:</td>
            <td>Pkl. 13.00 WITA s.d 21.00 WITA</td>
        </tr>
        <tr>
            <td>Lokasi</td>
            <td>:</td>
            <td>Klungkung Room<br> The Patra Hotel Resort and Villas <br> Jl.Ir. H. Juanda South Kuta Beach, Tuban, Kuta, Badung Regency, Bali 80361</td>
        </tr>
    </table>
    <br><br>
    Anda berkesempatan mendapatkan <i>"grandprize"</i> dengan melakukan <b><i>Checkin Registrasi</i></b> selama 2 (dua) hari acara dan <b>mengunjungi area exhibition/pameran</b>. <br><br>

    Kami informasikan hal-hal yang perlu diperhatikan oleh peserta :<br>
    a. Dresscode acara selama dua hari adalah smart casual <br>
    b. Harap membawa laptop di hari ke-2 untuk dapat mengikuti agenda <i>Technical Workshop</i>. <br><br>

    Berikut terlampir kami sampaikan agenda beserta informasi terkait rangkaian acara Konferensi ini.<br><br>

    <b>[REMINDER] :</b> Dimohon untuk mengikuti survey dari link berikut : <br><br>

    <a href="http://bit.ly/survei_rwp" class="btn btn-primary">Ikut Survey</a><br><br>

    Terima Kasih,<br>
    Admin IER Conference 2023
</div>

