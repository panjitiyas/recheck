<?php

namespace App\Http\Controllers;

use App\Exports\CheckExhibitionExport;
use App\Models\Checks;
use App\Models\Participant;
use Illuminate\Http\Request;
use App\Models\ParticipantIdCard;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ChecksController extends Controller
{
    public function index(){

        $where = 'checkin_time';
        $url = request()->segment(1);
        if($url =='exhib'){
            $where = 'check_in_pameran';
        }
        $data = DB::table('vCheckin')->orderBy($where,'DESC')->whereNotNull($where)->get();

        return DataTables::of($data)
        ->addIndexColumn()
        ->addColumn('waktu',function($data){
            $url = request()->segment(1);
            if($url === 'exhib'){
                return $data->check_in_pameran;
            } else {
                return $data->checkin_time;
            }
        })
        ->toJson();
    }

    public function getDataCheckByName (){
        $data = Participant::orderBy('id','ASC');
        return DataTables::eloquent($data)
        ->addIndexColumn()
        ->addColumn('aksi', function ($data) {
            return '<button id="bCheck" data-id='.$data->id.' class="btn btn-sm btn-dark">Pilih</button>';
        })->escapeColumns('aksi')
        ->toJson();
    }

    public function getDataParticipantforIdcard()
    {
        $data = ParticipantIdCard::orderBy('checkin_time','DESC');
        // dd($data);
        return DataTables::eloquent($data)
        ->addIndexColumn()
        ->addColumn('aksi', function ($data) {
            return view('pages.registrasi.komponen.tombol')->with('data', $data);
        })
        ->toJson();
    }

    public function takeIdcard(Request $request)
    {
            $code = 0;
            $message = 'Data gagal diupdate';
            $id = $request->id;
            $status =$request->code;
            $currentDateTime = date('Y-m-d H:i:s', time());
            $update =[
                'participant_id'=>$id,
                'checkin_time'=>$currentDateTime,
                'status'=>'ok',
                'user'=>Auth::user()->name,
            ];
            if (DB::table('check_idc')->where('participant_id',$id)->get()->count() == 0) {

                    DB::table('check_idc')->insert($update);
                    $code = 1;
                    $message = 'Data berhasil diupdate';

            } else {

                    DB::table('check_idc')->where('participant_id', $id)->delete();
                    $code = 1;
                    $message = 'Data berhasil diupdate';
            }

            $data = [
                'code'=>$code,
                'msg'=>$message
            ];

            return json_encode($data);
        }
    public function checkEvent (Request $request)
    {
            $code = 0;
            $message = 'Gagal Checkin';
            $id = $request->id;
            $currentDateTime = date('Y-m-d H:i:s', time());
            $currentDate = date('Y-m-d',strtotime($currentDateTime));
            $url = request()->segment(1);
            $cekPeserta = Participant::where('id',$id)->get()->count();
            // dd(Auth::user()->name);
        if($cekPeserta > 0){

            $cari = Checks::where('participant_id',$id)->get()->count();
            if($cari == 0){

                $checks = new Checks;
                $checks->participant_id = $id;
                if($url=='exhib'){
                    $checks->check_in_pameran=$currentDateTime;
                    $checks->category_id='3';
                } else {
                    $checks->checkin_time=$currentDateTime;
                    $checks->category_id='2';
                }
                $checks->status ='ok';
                $checks->user =Auth::user()->name;
                $checks->save();

                $code = 1;
                $message = 'Berhasil Check in';
            } else {
                $datapeserta = DB::table('checks')->where('participant_id',$id)->get()->last();
                $checkTime = $datapeserta->checkin_time;
                if($url=='exhib'){
                    $checkTime = $datapeserta->check_in_pameran;
                }
                if(date('Y-m-d',strtotime($checkTime))==$currentDate){
                    $user = Checks::where(['participant_id'=>$id,'user'=>Auth::user()->name])->get()->count();
                    // dd('salah');
                    if($user== 0){
                        $checks = new Checks;
                        $checks->participant_id = $id;
                        if($url=='exhib'){
                            $checks->check_in_pameran=$currentDateTime;
                            $checks->category_id='3';
                        } else {
                            $checks->checkin_time=$currentDateTime;
                            $checks->category_id='2';
                        }
                        $checks->status ='ok';
                        $checks->user =Auth::user()->name;
                        $checks->save();
                        $code = 1;
                        $message = 'Berhasil Check in';
                    } else {
                        $code = 0;
                        $message = 'Sudah Check in';
                    }
                } else {
                    $checks = new Checks;
                    $checks->participant_id = $id;
                    if($url=='exhib'){
                        $checks->check_in_pameran=$currentDateTime;
                        $checks->category_id='3';
                    } else {
                        $checks->checkin_time=$currentDateTime;
                        $checks->category_id='2';
                    }
                    $checks->status ='ok';
                    $checks->user =Auth::user()->name;
                    $checks->save();
                    $code = 1;
                    $message = 'Berhasil Check in';
                }
            }
        } else {
            $code = 0;
            $message = 'Data tidak ditemukan';
        }


        $data = [
            'code'=>$code,
            'msg'=>$message
        ];

        return json_encode($data);
    }

    public function ExportData(Request $request)
    {
        $tanggal ="all";
        if ($request->tanggal != null){
            $tanggal = date('Ymd',strtotime($request->tanggal));
        }

        // dd($request->tanggal);
        return Excel::download(new CheckExhibitionExport($tanggal),'data peserta_'.date('YmdHis',strtotime(now())).'.xlsx');
    }
}
