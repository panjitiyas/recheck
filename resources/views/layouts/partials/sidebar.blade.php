 <!--start sidebar -->
 <aside class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">
      <div>
        <img src={{ asset('assets/images/logo_rechek.png') }} class="logo-icon" alt="logo icon">
      </div>
      <div>
        <h4 class="logo-text"><b>Re</b>::Check</h4>
      </div>
      <div class="toggle-icon ms-auto"><i class="bi bi-chevron-double-left"></i>
      </div>
    </div>
    <!--navigation-->
    <ul class="metismenu" id="menu">
      <li class="{{ Request::is('/')?'mm-active':'' }}">
        <a href="{{ url('/') }}" >
          <div class="parent-icon"><i class="bi bi-house-door"></i>
          </div>
          <div class="menu-title">Dashboard</div>
        </a>
      </li>
      {{-- <li>
        <a href="javascript:;" class="has-arrow">
          <div class="parent-icon"><i class="bi bi-grid"></i>
          </div>
          <div class="menu-title">Application</div>
        </a>
        <ul>
          <li> <a href="app-emailbox.html"><i class="bi bi-arrow-right-short"></i>Email</a>
          </li>
          <li> <a href="app-chat-box.html"><i class="bi bi-arrow-right-short"></i>Chat Box</a>
          </li>
          <li> <a href="app-file-manager.html"><i class="bi bi-arrow-right-short"></i>File Manager</a>
          </li>
          <li> <a href="app-to-do.html"><i class="bi bi-arrow-right-short"></i>Todo List</a>
          </li>
          <li> <a href="app-invoice.html"><i class="bi bi-arrow-right-short"></i>Invoice</a>
          </li>
          <li> <a href="app-fullcalender.html"><i class="bi bi-arrow-right-short"></i>Calendar</a>
          </li>
        </ul>
      </li> --}}
      <li class="menu-label">Database</li>
      <li class="{{ Request::is('/participants')?'mm-active':'' }}">
          <a href="{{ url('participants') }}">
          <div class="parent-icon"><i class="bx bx-book-alt"></i>
          </div>
          <div class="menu-title">Participant</div>
        </a>
      </li>

      <li class="menu-label">Check In</li>
      <li>
        <a class="has-arrow" href="javascript:;">
          <div class="parent-icon"><i class="bi bi-file-earmark-break"></i>
          </div>
          <div class="menu-title">Registration</div>
        </a>
        <ul>
          <li> <a href="{{ url('regist/scanid') }}"><i class="bi bi-arrow-right-short"></i>Check In ID Card</a>
          </li>
          <li> <a href="{{ url('regist/scan') }}"><i class="bi bi-arrow-right-short"></i>Check In Event</a>
          </li>
          {{-- <li> <a href="{{ url('/fullsc') }}"><i class="bi bi-arrow-right-short"></i>Display Screen</a>
          </li> --}}
        </ul>
      </li>
      <li>
        <a class="has-arrow" href="javascript:;">
          <div class="parent-icon"><i class="bi bi-box-seam"></i>
          </div>
          <div class="menu-title">Exhibition Room</div>
        </a>
        <ul>
            <li> <a href="{{ url('exhib/scan') }}"><i class="bi bi-arrow-right-short"></i>Scanner Check</a>
            </li>
            {{-- <li> <a href="form-input-group.html"><i class="bi bi-arrow-right-short"></i>Display Screen</a>
            </li> --}}
          </ul>
      </li>
      {{-- <li>
        <a class="has-arrow" href="javascript:;">
          <div class="parent-icon"><i class="bi bi-cup"></i>
          </div>
          <div class="menu-title">Lounge Room</div>
        </a>
        <ul>
            <li> <a href="form-elements.html"><i class="bi bi-arrow-right-short"></i>Scanner Check</a>
            </li>
            <li> <a href="form-input-group.html"><i class="bi bi-arrow-right-short"></i>Display Screen</a>
            </li>
          </ul>
      </li>
      <li>
        <a class="has-arrow" href="javascript:;">
          <div class="parent-icon"><i class="bi bi-easel"></i>
          </div>
          <div class="menu-title">Conference Room</div>
        </a>
        <ul>
            <li> <a href="form-elements.html"><i class="bi bi-arrow-right-short"></i>Scanner Check</a>
            </li>
            <li> <a href="form-input-group.html"><i class="bi bi-arrow-right-short"></i>Display Screen</a>
            </li>
          </ul>
      </li> --}}
      {{-- <li class="menu-label">General Settings</li>
      <li>
        <a  href="{{ url('setting/user') }};">
          <div class="parent-icon"><i class="bi bi-easel"></i>
          </div>
          <div class="menu-title">User Management</div>
        </a>
      </li> --}}

    </ul>
    <!--end navigation-->
 </aside>
 <!--end sidebar -->
