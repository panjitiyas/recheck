 <!--plugins-->
 <link href={{ asset('assets/plugins/simplebar/css/simplebar.css') }} rel="stylesheet" />
 <link href={{ asset('assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }} rel="stylesheet" />
 <link href={{ asset('assets/plugins/metismenu/css/metisMenu.min.css') }} rel="stylesheet" />
 <link href={{ asset('assets/plugins/datatable/css/dataTables.bootstrap5.min.css') }} rel="stylesheet">

 <!-- Bootstrap CSS -->
 <link href={{ asset('assets/css/bootstrap.min.css') }} rel="stylesheet" />
 <link href={{ asset('assets/css/bootstrap-extended.css') }} rel="stylesheet" />
 <link href={{ asset('assets/css/style.css') }} rel="stylesheet" />
 <link href={{ asset('assets/css/icons.css') }} rel="stylesheet">
 <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
 <link rel="stylesheet" href={{ asset('assets/css/toastr.css') }}>
 <!-- loader-->
   <link href={{ asset('assets/css/pace.min.css') }} rel="stylesheet" />


 <!--Theme Styles-->
 <link href={{ asset('assets/css/dark-theme.css') }} rel="stylesheet" />
 <link href={{ asset('assets/css/light-theme.css') }} rel="stylesheet" />
 <link href={{ asset('assets/css/semi-dark.css') }} rel="stylesheet" />
 <link href={{ asset('assets/css/header-colors.css') }} rel="stylesheet" />

 <style>
  .page-item.active .page-link {
    background-color: #010711;!important
    border-color: #010711;!important
  }
 </style>