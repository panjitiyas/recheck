<?php

namespace App\Models;

use App\Models\Checks;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Participant extends Model
{
    use HasFactory;

    protected $guarded = [
        'created_at', 'updated_at'
    ];

    public function genders()
    {
        return $this->hasOne(Gender::class, 'id');
    }

    public function entities()
    {
        return $this->hasOne(Entity::class, 'id');
    }
    public function checks()
    {
        return $this->hasMany(Checks::class, 'id');
    }
}
