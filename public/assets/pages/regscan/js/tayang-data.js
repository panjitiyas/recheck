$(document).ready(function () {
    $("#tabel-idcard").DataTable({
        processing: true,
        serverside: true,
        ajax:'./data-idc',
        columns: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                orderable: false,
                searchable: false,
            },
            {
                data: "full_name",
                name:"nama",
            },
            {
                data: "company",
                name: "Perusahaan",
            },
            {
                data: "position",
                name: "Jabatan",
            },
            {
                data: "checkin_time",
                name:'checkin'

            },
            {
                data: "user",
                name:'user'

            },
            {
                data: "aksi",
                name: "aksi",
            },
        ],
        columnDefs: [
            {
                render: function (data, type, full, meta) {
                    return "<div class='text-wrap'>" + data + "</div>";
                },
                targets: [2,3]
            },
            {
                render: function (data, type, full, meta) {
                    if(data==null) return "<div class='text-center'> - </div>";
                    return "<div class='text-center'>" + data + "</div>";
                },
                targets: [0,4,5]
            }
         ]
    });


    $("#hpsBut").on("click",function(){
        let id = $(this).data('id')


        if (confirm("Apakah Anda yakin ingin menghapus data ini?")) {
            alert(id)
            // Kirim request DELETE ke server menggunakan AJAX
            // $.ajax({
            //     url: '/nama_resource/' + id,
            //     type: 'DELETE',
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     success: function(response) {
            //         // Tanggapan sukses dari server
            //         alert('Data berhasil dihapus');
            //         // Lakukan aksi lain yang diperlukan, misalnya refresh halaman atau hapus elemen dari DOM
            //     },
            //     error: function(xhr) {
            //         // Tanggapan error dari server
            //         alert('Terjadi kesalahan saat menghapus data');
            //     }
            // });
        }
    })
});
