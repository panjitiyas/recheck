<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeMail;
use App\Models\Participant;
use Illuminate\Http\Request;
use App\Events\EmailProgressEvent;
use Illuminate\Support\Facades\Mail;

class SendMailController extends Controller
{
    public function SendMail()
    {
        $code = 0;
        $message ='Tidak ada data untuk pengiriman email';
        $recipients = Participant::where('sts_email', '0') // Hanya email dengan status 0
        ->whereNotNull('email') // Hanya email yang tidak kosong
        ->take(200)
        ->get();
        $jumlahData = $recipients->count();
        // dd(json_encode($jumlahData));
        $hitungan = 0;
        if($jumlahData > 0){
            foreach ($recipients as $recipient) {

                $data = [
                    'attachment' => public_path('/upload/Term of Reference IER Conference 2023 for Participants.pdf')
                ];

                $email = new WelcomeMail($data);
                Mail::to($recipient->email)->send($email);

                Participant::where('id',$recipient->id)->update(['sts_email'=>'1']);
                $hitungan++;
                $progress =  round(($hitungan/$jumlahData)*100);
                event(new EmailProgressEvent($progress));
            }
            if($hitungan == $jumlahData){
                $code =1;
                $message ='Proses Berhasil';
            } else {
                $code=0;
                $message=$jumlahData - $hitungan.' data tidak berhasil diproses.';
            }
        }

        $data = array(
            'code'=>$code,
            'msg'=>$message
        );

        return json_encode($data);
    }

    public function checkProgressEmail(Request $request){
        return response()->json(['progress' => $request->session()->get('email_progress', 0)]);
    }


}
