<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        if (Auth::attempt($credentials)) {
            // Autentikasi berhasil
            return response()->json(['success' => true, 'level' => Auth::user()->role_id,'message'=>'Login berhasil ']);
        } else {
            // Autentikasi gagal
            return response()->json(['success' => false, 'message' => 'Gagal Login']);
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
