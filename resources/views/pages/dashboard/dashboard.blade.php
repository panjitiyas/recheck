@extends('layouts.app')

@section('content')
<div class="card shadow-sm radius-10 border-0 mb-3">
    <div class="card-header">
        <h4>GENERAL</h4>
    </div>
    <div class="card-body">
     @include('pages.dashboard.komponen.general')
    </div>
</div>
<div class="card shadow-sm radius-10 border-0 mb-3">
    <div class="card-header">
        <h4>DAILY</h4>
    </div>
    <div class="card-body">
      @include('pages.dashboard.komponen.daily')
    </div>
</div>
@endsection

@section('script')

@include('pages.dashboard.komponen.skrip')

@endsection
