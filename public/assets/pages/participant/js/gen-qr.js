$(function(){
    let progressBar = $('#progress-bar');
    $('#btnQr').on('click',function(){
       $('#icon-qr').addClass('spinner-border text-light text-sm')
       $('#icon-qr').removeClass('bx bx-play-circle')
       $.ajax({
        type:'GET',
        url:'/gen-qr',
        dataType:'json',
        success:function(data){
            if(data.code==1){
                toastr.success(data.msg);
                $("#tabel-peserta").DataTable().ajax.reload();
                $("#icon-qr").removeClass("spinner-border text-light text-sm");
                $("#icon-qr").addClass("bx bx-play-circle");
            } else{
                toastr.error(data.msg)
                $("#tabel-peserta").DataTable().ajax.reload();
                $("#icon-qr").removeClass("spinner-border text-light text-sm");
                $("#icon-qr").addClass("bx bx-play-circle");
            }
        },
        error:function(){
            toastr.error('Terjadi Kesalahan')
        }
       });
    })


})