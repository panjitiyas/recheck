<?php

namespace App\Imports;

use App\Models\Participant;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Validators\Failure;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;


class ParticipantImport implements ToModel, WithValidation, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function rules(): array
    {
        return [
            'full_name' => 'required',
            'sex' =>'required|numeric',
            'entity' => 'required|numeric',
            'company' => 'required',
            'position' => 'required',
            'wa_number' => 'required',
            // 'email' => 'required',
        ];
    }
    public function onFailure(Failure ...$failures)
    {
        foreach ($failures as $failure) {
            $errors[] = 'Row ' . $failure->row() . ': ' . $failure->errors()[0];
        }

        return redirect()->back()->with('errors', $errors);
    }
    public function model(array $row)
    {
        $lastId = Participant::orderBy('id', 'desc')->first();

        if($lastId==null){
            $currentId = 0;
        }else{
            $currentId = $lastId->uuid;
        }

        $newId = $currentId + 1;

        $uniqueNumber =  $newId;
        $folder = 'upload/qrCodes/'. $uniqueNumber.'_'.substr(str_replace(' ','_',$row['full_name']),0,10)  . '/';
        if (!File::exists(public_path($folder))) {
            File::deleteDirectory(public_path($folder));
            File::makeDirectory(public_path($folder),0777,null,true);
        }

        $genData = array(
            'name'=>$row['full_name'],
            'company'=>$row['company'],
            'position'=>$row['position'],
            'wa_number'=>$row['wa_number'],
            'email'=>$row['email'],
            'uuid'=>$uniqueNumber,
            'folder'=>$folder,
        );
        return new Participant([
            'uuid' => $uniqueNumber,
            'full_name' => ucwords(strtolower($row['full_name'])),
            'sex' => $row['sex'],
            'entity' => $row['entity'],
            'company' => $row['company'],
            'position' => $row['position'],
            'wa_number' => $row['wa_number'],
            'email' => strtolower($row['email']),
            'sts_email'=>0,
            'path_qrId' => $this->generateAndSaveQRCode($genData),
            'path_qrVc' => $this->generateQrVcard($genData)
        ]);
    }

    public function generateUniqueNumber()
    {
        do {
            $randomNumber = $this->generateRandomNumber();
        } while ($this->numberExistsInDatabase($randomNumber));

        return  $randomNumber;
    }

    private function generateRandomNumber()
    {
        return str_pad(mt_rand(0, 99999), 5, '0', STR_PAD_LEFT);
    }

    private function numberExistsInDatabase($number)
    {
        // Ganti UniqueNumber dan field_name dengan model dan nama kolom yang sesuai
        return Participant::where('uuid', $number)->exists();
    }

    public function generateAndSaveQRCode($data)
    {
        $path = $data['folder'];
        $filename = 'uuid_' . $data['uuid'] . '_' . substr(str_replace(' ', '_', Str::lower($data['name'])),0,16) . '.png';

        $file_path = $path . $filename;
        if (File::exists(public_path($file_path))) {
            File::delete(public_path($file_path));
        }
        // Generate QR Code
        QrCode::format('png')->size(300)->margin(5)->generate($data['uuid'], $file_path);

        // // Make Latar

        // $latar = Image::make(asset('assets/images/invit.png'));

        // // Membuka gambar QR code
        // $qrCodeImage = public_path($file_path);

        // // Menggabungkan gambar QR code dengan latar belakang
        // $latar->insert($qrCodeImage, 'bottom-left',270,110);

        // // Simpan gambar hasil
        // $path2 = $data['folder2'];
        // $filename2 = 'invit_' . $data['uuid'] . '_' . substr(str_replace(' ', '_', Str::lower($data['name'])), 0, 16) . '.png';
        // $file_path2 = $path2 . $filename2;
        // $latar->save($file_path2);

        return $filename;
    }

    public function generateQrVcard($data)
    {
        $folder = $data['folder'];
        $filename = 'vcard_' . $data['uuid'] . '_' . substr(str_replace(' ', '_', Str::lower($data['name'])),0,16) . '.png';

        $file_path = $folder . $filename;
        if (File::exists(public_path($file_path))) {
            File::delete(public_path($file_path));
        }

        $vcardString = 'BEGIN:VCARD' . PHP_EOL;
        $vcardString .= 'VERSION:3.0' . PHP_EOL;
        $vcardString .= 'N:' . $data['name'] . ';' . $data['name'] . ';;;' . PHP_EOL;
        $vcardString.= 'FN:'.$data['name']. PHP_EOL;
        $vcardString.= 'ORG:'.$data['company']. PHP_EOL;
        $vcardString.= 'TITLE:'.$data['position']. PHP_EOL;
        $vcardString .= 'EMAIL;TYPE=INTERNET:' . $data['email'] . PHP_EOL;
        $vcardString .= 'TEL;TYPE=CELL:' . $data['wa_number'] . PHP_EOL;
        $vcardString .= 'END:VCARD';

        // Generate QR Code
        QrCode::format('png')->size(300)->margin(5)->generate($vcardString, $file_path);


        return $filename;
    }


}
