<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParticipantIdCard extends Model
{
    use HasFactory;

    protected $table = 'vcheck';
}
