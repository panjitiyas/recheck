<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('full_name');
            $table->bigInteger('sex')->unsigned();
            $table->foreign('sex')->references('id')->on('genders');
            $table->bigInteger('entity')->unsigned();
            $table->foreign('entity')->references('id')->on('entities');
            $table->string('company');
            $table->string('position');
            $table->string('wa_number');
            $table->string('email');
            $table->string('qrId');
            $table->string('qrVc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        $table->dropForeign('participants_sex_foreign');
        $table->dropForeign('participants_entity_foreign');
        Schema::dropIfExists('participants');
    }
};
