<form id="formAdd" class="row g-3 p-3">
    <div class="col-12">
        <label class="form-label">Nama Lengkap</label>
        <input name="nama" type="text" class="form-control form-control-sm">
    </div>
    <div class="col-12">
        <label class="form-label">Jenis Kelamin</label>
        <select name="sex" type="text" class="form-select form-select-sm">
            <option value="1">Laki-laki</option>
            <option value="2">Perempuan</option>
        </select>
    </div>
    <div class="col-12">
        <label class="form-label">Entitas</label>
        <select name="entitas" type="text" class="form-select form-select-sm">
            <option value="1">BUMN, AP BUMN & Afiliasi</option>
            <option value="2">Kementerian/Lembaga Negara</option>
            <option value="3">Perusahaan Swasta</option>
            <option value="4">Asosiasi/NGO/Perkumpulan Akademisi</option>
            <option value="5">Lain-lain</option>
        </select>
    </div>
    <div class="col-12">
        <label class="form-label">Perusahaan/Company</label>
        <input name="company" type="text" class="form-control form-control-sm">
    </div>
    <div class="col-12">
        <label class="form-label">Jabatan di Perusahaan/Organisasi</label>
        <input name="jabatan" type="text" class="form-control form-control-sm">
    </div>
    <div class="col-12">
        <label class="form-label">Email</label>
        <input name="email" type="text" class="form-control form-control-sm">
    </div>
    <div class="col-12">
        <label class="form-label">Nomor Whatsapp</label>
        <input pattern="/^(\+62|0)[0-9]{9,13}$/" placeholder="masukan nomor Wa sesuai format :62XXX-XXXXXXXXXXXXX" name="noWa" type="tel" class="form-control form-control-sm">
    </div>
</form>