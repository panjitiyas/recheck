<div class="card shadow-sm border-0 overflow-hidden">
    <div class="card-body">
        <span class="badge bg-danger">belum ID Card</span>
        <div class="profile-avatar text-center">
            <img src="{{ asset('assets/images/avatars/male.png') }}" class="rounded-circle shadow" width="150" height="150" alt="">
        </div>

        <div class="text-center mt-4">
            <h4 class="mb-1">Panji Tiyas Pratama</h4>
            <div class="mt-4"></div>
            <h6 class="mb-1">Direktur Operasional</h6>
            <p class="mb-0 text-secondary">PT Idelogi Multimedia Indonesia</p>
        </div>
        <hr>
        <div class="text-start">
            <div class="card shadow-none border">
                <div class="card-header">
                  <h6 class="mb-0">INFORMASI KONTAK</h6>
                </div>
                <div class="card-body">
                  <form class="row g-3">
                    <div class="col-12">
                      <label class="form-label">Email</label>
                      <input type="text" class="form-control" value="panjitiyas@gmail.com" readonly>
                     </div>
                     <div class="col-12">
                        <label class="form-label">Nomor Whatsapp</label>
                        <input type="text" class="form-control" value="+62856 43040398" readonly>
                     </div>

                  </form>
                </div>
              </div>
        </div>
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item d-flex justify-content-center align-items-center bg-transparent border-top">
            <a href="#" class="btn btn-lg btn-dark">Registrasi</a>
        </li>
    </ul>
</div>