<script src={{ asset('assets/js/jquery.min.js') }}></script>
 <!-- Bootstrap bundle JS -->
 <script src={{ asset('assets/js/bootstrap.bundle.min.js') }}></script>
 <!--plugins-->
 <script src="//cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
 <script src={{ asset('assets/plugins/simplebar/js/simplebar.min.js') }}></script>
 <script src={{ asset('assets/plugins/metismenu/js/metisMenu.min.js') }}></script>
 <script src={{ asset('assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js') }}></script>
 <script src={{ asset('assets/js/pace.min.js') }}></script>
 <script src={{ asset('assets/plugins/easyPieChart/jquery.easypiechart.js') }}></script>
 <script src={{ asset('assets/js/toastr.js')}}></script>
 <!--app-->
 <script src={{ asset('assets/js/app.js') }}></script>
 <script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
 </script>

