$(document).ready(function () {
    $("#tabel-peserta").DataTable({
        processing: true,
        serverside: true,
        ajax:'./participant',
        columns: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                orderable: false,
                searchable: false,
            },
            {
                data: "full_name",
                name:"nama",
            },
            {
                data: "company",
                name: "Perusahaan",
            },
            {
                data: "position",
                name: "Jabatan",
            },
            {
                data: "wa_number",
                name:'Nomor WA'

            },
            {
                data: "email",
                name:'email'

            },
            {
                data: "aksi",
                name: "aksi",
            },
        ],
        columnDefs: [
            {
                render: function (data, type, full, meta) {
                    return "<div class='update text-wrap'>" + data + "</div>";
                },
                targets: [1,2,3]
            },
            {
                render: function (data, type, full, meta) {
                    return "<div class='update text-center'>" + data + "</div>";
                },
                targets: [0,4,6]
            }
         ]
    });




$('#tabel-peserta').on('click','.btnEdit',function(){
    let id = $(this).data('id')
    $.ajax({
        type: "GET",
        url: "/participant/"+id+"/edit",
        success: function (res) {
            $('#modalEdit').find('#mdContent').html(res)
            $('#modalEdit').modal('show')

        }
    });
})

$('#modalEdit').on('click','#editButton',function(e){
    e.preventDefault()
    let dataForm = $('#formEdit')
    let id =dataForm.find('input:hidden').val()
    $.ajax({
        type: "PUT",
        url: "participant/"+id,
        data: dataForm.serialize(),
        dataType: "json",
        success: function (data) {
            id = 0
            if(data.code==0){
                toastr.error(data.msg)
            } else {
                toastr.success(data.msg)
                setTimeout(function() {
                    $('#tabel-peserta').DataTable().ajax.reload()
                    $("#mdContent").empty();
                    dataForm[0].reset()
                    $("#modalEdit").modal("hide")
                }, 1500);
            }
        }
    });
})


    $("#tabel-peserta").on("click","#hpsBut",function(){
        let id = $(this).data('id')
        $('#hapusBtn').attr('data-id',id)
        $('#hapusBtn').on('click',function(){
           $.ajax({
            type: "DELETE",
            url: "/participant/"+id,
            dataType: "json",
            success: function (data) {
                if(data.code==1){
                    toastr.success(data.msg)
                    setTimeout(function (){
                        $("#tabel-peserta").DataTable().ajax.reload();
                        $('#hapusModal').modal("hide")
                    }, 1000);
                } else {
                    toastr.error(data.msg)
                }

            }
           });
        })
    })
});
