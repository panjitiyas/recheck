<!DOCTYPE html>
<html lang="en" class="semi-dark headercolor2">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href={{ asset('assets/images/favicon-32x32.png') }} type="image/png" />
    @include('layouts.css.main_css')
    <title>Re: :Check</title>
    {{-- @stack('scripts') --}}
</head>
<body>
    <div class="wrapper">
        {{-- TOP HEADER --}}
        @include('layouts.partials.topbar')
        {{-- SIDEBAR --}}
        @include('layouts.partials.sidebar')
        {{-- MAIN --}}
        <main class="page-content">
            {{-- Content Here --}}
           @yield('content')
        </main>

        <!--start overlay-->
        <div class="overlay nav-toggle-icon"></div>
       <!--end overlay-->

        <!--Start Back To Top Button-->
        <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
        <!--End Back To Top Button-->


    </div>

    @include('layouts.js.main_js')
    @yield('script')
</body>
</html>
