@extends('layouts.app')
@section('content')
<div class="row ${1| ,row-cols-2,row-cols-3, auto,justify-content-md-center,|}">
    <div class="col-12">
        <h5>REGISTRASI</h5>
    </div>
</div>
<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="ms-auto position-relative">
                <div class="position-absolute top-50 translate-middle-y search-icon fs-5 px-3"><i class="bx bx-scan"></i></div>
                <input id="regScan" name="uuid" class="form-control form-control-lg ps-5" type="text" placeholder="Pindai QR">
            </div>
            <!--end row-->
        </div>
    </div>
</div>

<div class="row">
    {{-- PROFIL --}}
    <div class="col-12 col-sm-6" id="profil"></div>

    {{-- REKAM AKTIVITAS --}}
    <div class="col-12 col-sm-6" id="history"></div>
</div>


@endsection

@section('script')
    @include('pages.registrasi.script')
@endsection

