<form id="formEdit" class="row g-3 p-3">
    <div class="col-12">
        <label class="form-label">Nama Lengkap</label>
        <input type="hidden" name="id" value="{{ $data->id }}">
        <input name="nama" type="text" value="{{  $data->full_name}}" class="form-control form-control-sm">
    </div>
    <div class="col-12">
        <label class="form-label">Jenis Kelamin</label>
        <select name="sex" type="text" class="form-select form-select-sm">
            <option value="1" {{ $data->sex == "1" ?"selected":"" }}>Laki-laki</option>
            <option value="2" {{ $data->sex == "2" ?"selected":"" }}>Perempuan</option>
        </select>
    </div>
    <div class="col-12">
        <label class="form-label">Entitas</label>
        <select name="entitas" type="text" class="form-select form-select-sm">
            <option value="1" {{ $data->sex == "1" ?"selected":"" }}>BUMN, AP BUMN & Afiliasi</option>
            <option value="2" {{ $data->sex == "2" ?"selected":"" }}>Kementerian/Lembaga Negara</option>
            <option value="3" {{ $data->sex == "3" ?"selected":"" }}>Perusahaan Swasta</option>
            <option value="4" {{ $data->sex == "4" ?"selected":"" }}>Asosiasi/NGO/Perkumpulan Akademisi</option>
            <option value="5" {{ $data->sex == "5" ?"selected":"" }}>Lain-lain</option>
        </select>
    </div>
    <div class="col-12">
        <label class="form-label">Perusahaan/Company</label>
        <input name="company" type="text" value="{{$data->company}}" class="form-control form-control-sm">
    </div>
    <div class="col-12">
        <label class="form-label">Jabatan di Perusahaan/Organisasi</label>
        <input name="jabatan" type="text" value="{{ $data->position }}" class="form-control form-control-sm">
    </div>
    <div class="col-12">
        <label class="form-label">Email</label>
        <input name="email" type="text" value="{{$data->email  }}" class="form-control form-control-sm">
    </div>
    <div class="col-12">
        <label class="form-label">Nomor Whatsapp</label>
        <input pattern="/^(\+62|0)[0-9]{9,13}$/" placeholder="masukan nomor Wa sesuai format :62XXX-XXXXXXXXXXXXX" name="noWa" value="{{$data->wa_number  }}" type="tel" class="form-control form-control-sm">
    </div>
</form>