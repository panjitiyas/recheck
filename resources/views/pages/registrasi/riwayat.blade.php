<div class="card">
    <div class="card-body">
        <div class="d-flex align-items-center">
            <div>
                <h5 class="mb-0">RIWAYAT AKTIVITAS PESERTA</h5>
            </div>
        </div>
        <div class="table-responsive mt-3">
            <table id="tabel-aktivitas" class="table table-hover mb-0">
                <thead class="table-light text-center">
                    <tr>
                        <th>Aktivitas</th>
                        <th>Tanggal Rekam</th>
                        <th>Waktu Rekam</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>