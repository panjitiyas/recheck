<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        body{
            background-attachment: fixed;
            background-image: url({assets('assets/images/bg.png')});
            background-size: cover;
            overflow: hidden;
            min-height:100vh
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12" >
                <img style="overflow: hidden;" src={{ asset('assets/images/bg.png') }} alt="" class="img-fluid" width="100%" height="100%">
            </div>
        </div>
    </div>
</body>
</html>