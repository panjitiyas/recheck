<div class="row row-cols-1 row-cols-md-2 row-cols-lg-2 row-cols-xl-4">
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <p class="mb-0 text-secondary">Total Peserta</p>
                        <h4 class="my-1" id="gtptc"></h4>
                        <p class="mb-0 text-success font-13">orang</p>
                    </div>
                    <div class="widget-icon-large bg-gradient-purple text-white ms-auto"><i class="bi bi-people-fill"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <p class="mb-0 text-secondary">Pengambilan ID Card</p>
                        <h4 class="my-1" id="gtidc"></h4>
                        <p class="mb-0 text-success font-13">orang</p>
                    </div>
                    <div class="widget-icon-large bg-gradient-success text-white ms-auto"><i class="bi bi-person-badge-fill"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <p class="mb-0 text-secondary">Checkin Registrasi</p>
                        <h4 class="my-1" id="gtreg"></h4>
                        <p class="mb-0 text-success font-13">orang</p>
                    </div>
                    <div class="widget-icon-large bg-gradient-danger text-white ms-auto"><i class="bi bi-person-check-fill"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <p class="mb-0 text-secondary">Checkin Exhibition</p>
                        <h4 class="my-1" id="gtexh"></h4>
                        <p class="mb-0 text-success font-13">orang</p>
                    </div>
                    <div class="widget-icon-large bg-gradient-info text-white ms-auto"><i class="bi bi-person-bounding-box"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-start">
    <div class="col-md-2 col-12 ">
        <a class="btn btn-sm btn-dark d-block" id="fetchData"><i class="bi bi-arrow-clockwise"></i> Refresh Data</a>
    </div>
</div>

