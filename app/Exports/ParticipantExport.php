<?php

namespace App\Exports;

use App\Models\Participant;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ParticipantExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return DB::table('vParticipant')->orderBy('id','asc')->get();
    }

    public function map($participant): array
    {
        $idCPath =             public_path('upload/qrCodes/' . substr($participant->uuid, 7, 5) . substr(str_replace(' ', '_', $participant->full_name), 0, 10) . '/' . $participant->path_qrId);
        $vcPath =             public_path('upload/qrCodes/' . substr($participant->uuid, 7, 5) . substr(str_replace(' ', '_', $participant->full_name), 0, 10) . '/' . $participant->path_qrVc);


        return [
            $participant->uuid,
            $participant->full_name,
            $participant->gender,
            $participant->entitas,
            $participant->company,
            $participant->position,
            $participant->wa_number,
            $participant->email,
            $participant->path_qrId,
            $participant->path_qrVc

        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'Nama',
            'Jenis Kelamin',
            'Entitas',
            'Perusahaan/Organisasi',
            'Jabatan',
            'Nomor WA',
            'Email',
            'QR ID',
            'QR VCARD'
        ];
    }
    public function styles(Worksheet $sheet)
    {
        // Menetapkan style untuk kolom gambar (misalnya, lebar dan tinggi)
        $sheet->getColumnDimension('J')->setWidth(30);
    }
    public function columnFormats(): array
    {
        return [
            'J' => 'html',
        ];
    }

    public function drawing($image)
    {
        $drawing = new Drawing();
        $drawing->setPath($image);
        $drawing->setHeight(30);

        return $drawing;
    }
}
