<?php

namespace App\Http\Controllers;

use App\Helpers\CustomHelper as CustomHelp;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('pages.dashboard.dashboard');
    }

    public function generalStats()
    {
        $count = array (
            'ptc'=>CustomHelp::getTotalParticipant(),
            'idc'=>CustomHelp::getTotalIdc(),
            'reg'=>CustomHelp::getTotalReg(),
            'exh'=>CustomHelp::getTotalExh(),
        );

        return json_encode($count);

    }
    public function dailyStats(Request $request)
    {


        if ($request->tanggal!='all' && $request->tanggal!='') {
            $tanggal = date('Y-m-d', strtotime($request->tanggal));
            $count = array (
                'idc'=>CustomHelp::getTotalIdc($tanggal),
                'reg'=>CustomHelp::getTotalReg($tanggal),
                'exh'=>CustomHelp::getTotalExh($tanggal),
            );
        } else {
            $count = array (
                'idc'=>CustomHelp::getTotalIdc(),
                'reg'=>CustomHelp::getTotalReg(),
                'exh'=>CustomHelp::getTotalExh(),
            );
        }
        // dd(json_encode($count));

        return json_encode($count);

    }
}
